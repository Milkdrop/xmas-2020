intro = "Welcome back, we've fixed the problem with the key generation you found in our system.\nNow we're facing another problem, we seem to be having a power outgage, and our machines are running undervoltaged, hopefully that's not a problem.\nCan you please take a look again at our protocol and try to find some vulnerability?\nOh, by the way, you only have {} actions.\nHere's the public key, you'll be needing that!\nn:{}\ne:{}\n"

menu = "Choose what you want to do:\n1. sign message\n2. forge signature\n3. exit\n"

invalid_input = "Invalid input. Aborting!\n"
bad_input = "That is not a correct signature. Aborting!\n"

get_msg = "Let's sign something for you.\n"
show_signature = "Here's the signature: {}\n"
show_forgery = "Give me the signature for this following message: {}\n"

goodbye = "Arrivederci fratello!"

win = "Congratulations, you trully are a great hacker!\nHere's your flag: {}"
FLAG = "X-MAS{Oh_CPU_Why_h4th_th0u_fors4k3n_u5_w1th_b3llc0r3__th3_m4th_w45_p3rf3c7!!!_2194142af19aeea4}"
