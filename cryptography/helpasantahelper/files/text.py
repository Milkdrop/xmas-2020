intro = "Hello, thanks for answering my call, this time we have to break into Krampus' server and recover a stolen flag.\nWe have to solve a hash collision problem to get into the server.\nSadly, we're on a hurry and you only have {} actions you can make until we get detected."

menu = "Choose what you want to do:\n1. hash a message\n2. provide a collision\n3. exit\n"

hash_message = "Ok, give me a hex-encoded string to hash.\n"
collision_message = "Now give me two different hex-encoded strings to check for a collision.\n"

invalid_input = "Invalid input. Aborting!\n"
bad_input = "The input you gave me is not hex-encoded. Aborting!\n"

show_hash = "Here is your hash: {}.\n"

goodbye = "Sad to see you leave, I was eager to get that flag."
lose = "Oh, you silly, that was not it, you should try one more time!"

get_msg = "Give me a message."

win = "Damn, that was a really clever approach, you should be proud of yourself.\nHere's the flag: {}"
FLAG = "X-MAS{C0l1i5ion_4t7ack5_4r3_c0o1!_4ls0_ch3ck_0u7_NSUCRYPTO_fda233}"
