intro = "Welcome to Santa's public key factory, here we create the world's highest quality keys.\nSince this is an annanounced visit you can look through at most {} keys.\nAs a bonus, try to use those keys to find what Santa's secret message for you is."

menu = "Choose what you want to do:\n1. get encrypted secret message\n2. guess the secret message\n3. exit\n"

invalid_input = "Invalid input. Aborting!\n"
bad_input = "That was not the secret message. Aborting!\n"

guess_msg = "Let's see what you've got.\n"
enc_flag = "Here is the encrypted secret message: {}.\nAh, and also here's the public key with which it was encrypted:\nn: {}\ne: {}\n"

goodbye = "Farewell! Hope we will meet again soon."

win = "It seems you are a genius, we can't understand how you did it, but you did.\nHere's your flag: {}"
FLAG = "X-MAS{M4yb3_50m3__m0re_r4nd0mn3s5_w0u1d_b3_n1ce_eb0b0506}"
