#include <stdio.h>
#include <random>
#include <time.h>
#include <cstring>
#include "MMU.h"
#include "CPU.h"

void processByteStream(u8* buffer);

int main(int argc, char** argv) {
	srand(time(NULL));

	printf("CP/M-1337 on the Intel 8080\t1 Dec 89\n");
	printf("\t(80 column display terminal emulator)\n\n");
	printf("==========================\n\
== GN0M3 SYSADMIN NOTES ==\n\
==========================\n\
\n\
Finally! We are putting our old 8080 chips to good use!\n\
- We have an entire 64K rank to play with, divided into 8 banks of 16 rows of 512B each.\n\
- Kernel is supposed to block R/W access on addresses outside the allocated memory page of an unprivileged process (The interrupt vector table is an exception).\n\
- When an out-of-bounds R/W occurs, the kernel should kill the process via machine shutdown. (Hopefully!)\n\
- The kernel keeps its page table information (alongside privilege data) in the kernel work RAM (200h - 400h)\n\
- This system has NO CACHE YET! Programs may run really slowly. (Blame the laughable Lapland budget for this)\n\
- Every test machine has critical synchronization information stored at 0x300. Do NOT touch this. Synchronization information is available only to gnomes with level 3 sysadmin clearance. PERIOD!\n\
- We currently have two Syscalls:\n\
\t- Reset 0: Should quit back to BIOS. This will terminate your program.\n\
\t- Reset 1: Prints things out to serial. Pretty useful for debugging. If register C is 2 then it prints out the ASCII value in register E. If register C is 9 then it prints out the $-terminated string starting from the address stored in register DE. Undefined behaviour otherwise.\n\
\n\
PS: For Clark and Jebediah, we have limited the number of instructions you can run on a single instance to 0x4FFFFFFF :-) Sorry guys, you can't play Lunar Lander on our testing systems! We have dedicated arcades in our lounge for that. Stay in line like the rest of us!\n\
\n\
==========================\n\n");

	int startAddress = 0x0100 * (4 + rand() % 4) + ((rand() % 0x100) / 2) * 2;
	
	printf("You have been allocated the following 4096B page in memory:\n");
	printf("%04xh-%04xh\n\n", startAddress, startAddress + 0x1000);
	printf("Input your 8080 binary to be loaded at the start of the page\n");
	printf("(Format = Hex-encoded byte stream): 001122DEAD9A...\n");
	fflush(stdout);

	u8 buffer[4096 * 2 + 1];
	memset(buffer, 0, 4096 * 2 + 1);

	fgets((char*) buffer, 4096 * 2, stdin);
	processByteStream(buffer);

	MMU mmu(startAddress, startAddress + 0x1000);
	CPU cpu(&mmu);

	mmu.LoadInMemory(buffer, startAddress, 4096);

	while (!cpu.Halt) {
		cpu.Clock();
	}

	printf("Machine shutdown.\n");
}

void processByteStream(u8* buffer) {
	for (int i = 0; i < 4096 * 2; i += 2) {
		u8 value = 0;

		char c = buffer[i];
		if (c >= '0' && c <= '9') value += (c - '0') * 0x10;
		if (c >= 'A' && c <= 'F') value += (c - 'A' + 10) * 0x10;
		if (c >= 'a' && c <= 'f') value += (c - 'a' + 10) * 0x10;

		c = buffer[i + 1];
		if (c >= '0' && c <= '9') value += c - '0';
		if (c >= 'A' && c <= 'F') value += c - 'A' + 10;
		if (c >= 'a' && c <= 'f') value += c - 'a' + 10;

		buffer[i / 2] = value;
	}
}