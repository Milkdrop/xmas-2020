#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <random>
#include "MMU.h"

MMU::MMU(u16 _pageStart, u16 _pageEnd) {
	memset (Memory, 0, sizeof(Memory));
	pageStart = _pageStart;
	pageEnd = _pageEnd;
	isPrivileged = 0;
	forcePrivileged = 0;

	for (int i = 0x0000; i < 0x10000; i++) { // ROM + RAM + VRAM
		MemoryMap[i] = Memory + i;
	}

	strcpy((char*) Memory + 0x300, "X-MAS{Row_r0w_ROWHAMMER_y0ur_b0at!_G3ntly_d0wn_th3_r0wstr34m_41ec3145}\n$");
}

u8 MMU::GetByteAt(u16 addr) {
	if (!isValidAddress(addr)) {
		printf ("Illegal address access: 0x%04x\n", addr);
		exit(0);
	}

	CheckBitFlip(addr);
	return *MemoryMap[addr];
}

u16 MMU::GetWordAt(u16 addr) {
	if (!isValidAddress(addr)) {
		printf ("Illegal address access: 0x%04x\n", addr);
		exit(0);
	}

	CheckBitFlip(addr);
	return *MemoryMap[addr] | (*MemoryMap[addr + 1] << 8);
}

void MMU::SetByteAt(u16 addr, u8 value) {
	if (!isValidAddress(addr)) {
		printf ("Illegal address access: 0x%04x\n", addr);
		exit(0);
	}

	*MemoryMap[addr] = value;
	CheckBitFlip(addr);
}

void MMU::SetWordAt(u16 addr, u16 value) {
	if (!isValidAddress(addr)) {
		printf ("Illegal address access: 0x%04x\n", addr);
		exit(0);
	}

	*MemoryMap[addr] = value & 0xFF;
	*MemoryMap[addr + 1] = value >> 8;
	CheckBitFlip(addr);
}

u8 MMU::isValidAddress(u16 addr) {
	return (addr >= pageStart) || isPrivileged;
}

void MMU::LoadInMemory(u8* buffer, u16 addr, u16 size) {
	memcpy(Memory + addr, buffer, size);
}

void MMU::CheckBitFlip(u16 addr) {
	if (lastRow == addr / 0x200) {
		return;
	}

	lastRow3 = lastRow2;
	lastRow2 = lastRow;
	lastRow = addr / 0x200;
	if ((lastRow == 0 || lastRow2 == 0 || lastRow3 == 0)
		&& (lastRow == 2 || lastRow2 == 2 || lastRow3 == 2)) {
		bitflipCounter++;
	}

	memaccessCounter++;

	if (memaccessCounter == 0x014FFFFF) {
		memaccessCounter = 0;
		u8 chance = rand() % 32;

		if (chance <= 10 && lastRow != 0x7F) {
			u16 flipAddr = (lastRow + 1) * 0x200;
			if ((lastRow2 == lastRow - 2 || lastRow3 == lastRow - 2) && lastRow != 0) {
				flipAddr = (lastRow - 1) * 0x200;
			}

			flipAddr += rand() % 0x200;
			u16 bit = 1 << (rand() % 8);
			isPrivileged = 1;
			SetByteAt(flipAddr, GetByteAt(flipAddr) ^ bit);
			isPrivileged = 0;
		}
	}

	//printf("%d %d %d\n", lastRow, lastRow2, lastRow3);
	if (bitflipCounter == 0x014FFFFF) {
		InduceBitFlip();
	}
}

void MMU::InduceBitFlip() {
	bitflipCounter = 0;
	u8 chance = 0;

	if ((lastRow - lastRow2 == 2 || lastRow2 - lastRow == 2)
		|| (lastRow - lastRow3 == 2 || lastRow3 - lastRow == 2)
		|| (lastRow2 - lastRow3 == 2 || lastRow3 - lastRow2 == 2)
	) {
		chance = rand() % 32;
	} else {
		chance = rand() % 48;
	}

	if (chance == 0) {
		isPrivileged = 1;
		forcePrivileged = 1;
	}

	if (chance >= 20 && chance < 48) {
		for (int i = 0; i < 8 + rand() % 256; i++) {
			printf("%c", rand() % 256);
		}

		printf("\n\n\nMemory corruption detected.\nMachine shutdown.\n");
		exit(0);
	}
}