import time
import sys

balance = 0
def printBalance():
	global balance
	if balance.imag == 0.0:
		b = balance.real
		print(f"Balance: ${b}")
	else:
		print(f"Balance: ${balance}")


def confiscate():
	global balance
	if balance.real > 0.0:
		balance -= complex(balance.real)


def month(monthNo, confiscator):
	global balance
	print()
	print(f"Month {monthNo}")
	print("-------")
	balance *= intr
	printBalance()
	print(f"Your account has been reported by an anonymous user. {confiscator} confiscated your funds.")
	confiscate()
	printBalance()
	time.sleep(1.5)


banner = """#########################
#  _____  __  __ ____   #
# |  __ \|  \/  |  _ \  #
# | |__) | \  / | |_) | #
# |  ___/| |\/| |  _ <  #
# | |    | |  | | |_) | #
# |_|    |_|  |_|____/  #
#                       #
#########################"""

print(banner)
print("Welcome to our hacker-friendly terminal service!")
print("This program is a part of our 'friendlier products for hackers' initiative.")
print("Please select an action:")
print("1. Open account")
print("2. Report account")
time.sleep(0.25)
print("*you select 1*")
time.sleep(0.5)
print("That's great! As you probably know, our bank provides anonymous choose-your-interest-rate accounts.")
print("Please keep in mind that the interest rate can be *any* number as long as its modulus is less than 100.")
print("Interest:", end=" ")

interest = input()

try:
	intr = complex(interest)
except:
	print("That interest rate does not look like a number!")
	sys.exit(1)

if abs(intr) >= 100:
	print("The modulus is equal to or greater than 100. Exiting...")
	sys.exit(1)

time.sleep(0.25)
print("Creating your account...")
time.sleep(0.5)
print("*your account has been created*")
time.sleep(1)
print("Received $1000 with attached note: 'ransom payment'")
balance += 1000
printBalance()
time.sleep(0.5)
print("*request money withdrawal*")
time.sleep(0.25)
print("*error: your account must be at least 4 months old*")
time.sleep(1.5)

month(1, "CIA")
month(2, "FBI")
month(3, "INTERPOL")

print()
print("Month 4")
print("-------")
balance *= intr
printBalance()
print("*you want to buy a flag for $10 million*")

if balance.real >= 10000000:
	print("GG!")
	print("X-MAS{th4t_1s_4n_1nt3r3st1ng_1nt3r3st_r4t3-0116c512b7615456}")
else:
	print("ERROR: Not enough funds.")
