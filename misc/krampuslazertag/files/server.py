import os
import random
from math import *
from time import sleep
from threading import Thread
flag = "X-MAS{Wh3n_11F3_5h0Ot5_14Z3r5_a7_y0U_28f901ab}"

long_bad_story = """
You have fallen into Krampus' trap again... but this time he has a secret weapon. He stole StyrofoamPyromaniac's LAZER
BAZOOKA. When you look around you also see that you and Krampus are in a square room with walls made out of perfect 
LAZER mirrors (they only reflect LAZERs). The floor and ceiling are made from a material that does not reflect LAZERs.

With the help of your LAZER knowledge you notice something weird... Krampus has the wrong beam collimator on his bazooka.
He is using the ELFTeK 6000, a beam collimator that uses Christmas magic to determine the middle point of the beam's 
length and make it converge at that point.

Because of plot armor, Krampus is immune to his LAZER. Desperately looking for something to get you out of this 
situation, you find a box of 16 ELFTeK 640 LAZER blockers, the problem is that they are only big enough to block the
LAZER when it has converged into a point.

Because Krampus has some back issues he can only hold the BAZOOKA parallel to the XY plane.

You have 30 seconds to put the blockers in the right place for 50 rounds.

You need to give the X and Y coordinates for each of the blockers separated by a comma. The blockers are separated by a new line.

Good luck! 

"""


def die(seconds):
    sleep(seconds)
    print("TOO SLOW!")
    os._exit(0)


def compare_points(p1, p2):
    x1, y1 = p1
    x2, y2 = p2

    if isclose(x1, x2, abs_tol=0.0001) and \
            isclose(y1, y2, abs_tol=0.0001):
        return True
    return False


def gen_krampus_position():
    return random.uniform(0.00001, 0.29999997), random.uniform(0.00001, 0.999996)


def gen_player_position():
    return random.uniform(0.7999993, 0.99999997), random.uniform(0.00001, 0.999996)


def get_midpoint(x1, y1, x2, y2):
    return (x1 + x2) / 2, (y1 + y2) / 2


def bring_to_origin(x1, y1):
    while x1 > 1:
        x1 = x1 - 2 * (x1 - floor(x1))
    while y1 > 1:
        y1 = y1 - 2 * (y1 - floor(y1))
    return x1, y1


def bring_from_origin_to_box_nm(x1, y1, n, m):
    while x1 < n:
        x1 = x1 + 2 - 2 * (x1 - floor(x1))
    while y1 < m:
        y1 = y1 + 2 - 2 * (y1 - floor(y1))
    return x1, y1


def play_round():
    solution = []

    Sx, Sy = gen_player_position()
    Tx, Ty = gen_krampus_position()
    print(f'Your position: {Sx},{Sy}')
    print(f'Krampus\' position: {Tx},{Ty}')
    for i in range(4):
        for j in range(4):
            x_p, y_p = bring_from_origin_to_box_nm(Tx, Ty, i, j)
            x_p, y_p = get_midpoint(Sx, Sy, x_p, y_p)
            x_p, y_p = bring_to_origin(x_p, y_p)
            solution.append((x_p, y_p))

    user_solution = []
    for i in range(16):
        user_input = input().split(",")
        point = (0, 0)
        try:
            point = (float(user_input[0]), float(user_input[1]))
        except:
            print("Wrong!")
            exit(-1337)
        user_solution.append(point)

    solution_copy = list(solution)

    for good_point in solution:
        for user_point in user_solution:
            if compare_points(good_point, user_point):
                solution_copy.remove(good_point)

    if len(solution_copy) != 0:
        return False
    return True


def game():
    print(long_bad_story)
    input("Send any input to start...")
    stop = Thread(target=die, args=(30,))
    stop.start()
    rounds = 50
    for i in range(1, rounds + 1):
        print(f'Round {i}:')
        if not play_round():
            print("Wrong!")
            sleep(1)
            sys.stdout.flush()
            os._exit(0)
    print("Congrats!")
    print(flag)
    sys.stdout.flush()


if __name__ == "__main__":
    game()
    sleep(1)
    os._exit(0)
