#ifndef CPU_H
#define CPU_H
#include "utils.h"
#include "charset.h"
#include "gpu.cpp"
#include <chrono>

using namespace std::chrono;

class CPU {
public:
	u8 halted;
	u8 debug = 0;

	CPU(GPU* gpu, u8* tape, u32 tapeSize) {
		this->gpu = gpu;
		this->tape = tape;
		this->tapeSize = tapeSize;

		pc = 0;
		flag = 0;
		halted = 0;
		memset(r, 0, sizeof(r));

		memset(readBuffer, 0, sizeof(readBuffer));
		readBufferIndex = 0;
		readBufferSize = 0;

		addr = 0;
		memset(mem, 0, sizeof(mem));

		lastTick = 0;
		lastInteract = 0;
	}

	void clock() {
		u64 currentTime = getCurrentTime();
		clockTick(currentTime);

		if (currentTime > lastInteract + 33) {
			lastInteract = currentTime;
			gpu->render();
			processInput();
		}

		u32 instruction = tape[pc] * 0x10000 + tape[pc + 1] * 0x100 + tape[pc + 2];
		
		u8 opc = (instruction & (0x3F << (6 * 3))) >> (6 * 3);
		u8 d = (instruction & (0x3F << (6 * 2))) >> (6 * 2);
		u8 a = (instruction & (0x3F << (6 * 1))) >> (6 * 1);
		u8 b = instruction & (0x3F);

		if (opc == 0) {
			halted = 1;
		}

		u8 condition = (opc - 1) / 21;
		opc = (opc - 1) % 21;

		if (condition == 1 && flag == 0) opc = 0xFF;
		if (condition == 2 && flag == 1) opc = 0xFF;

		if (debug && opc != 0xFF) {
			printf("%d %d: (%d: %o) %o %o %o\n", flag, pc, condition, opc, d, a, b);
		}

		switch (opc) {
			// Don't execute this instruction
			case 0xFF: break;

			// Arithmetic and Logic
			case 000: r[d] = r[a] + r[b]; break;
			case 001: r[d] = r[a] + b; break;
			case 002: r[d] = r[a] - r[b]; break;
			case 004: r[d] = r[a] | r[b]; break;
			case 005: r[d] = r[a] | b; break;
			case 006: r[d] = r[a] ^ r[b]; break;
			case 007: r[d] = r[a] ^ b; break;
			case 010: r[d] = r[a] & r[b]; break;
			case 011: r[d] = r[a] & b; break;
			case 013: r[d] = r[a] << r[b]; break;
			case 014: r[d] = r[a] >> r[b]; break;

			// Comparison
			case 003: {
				u8 cmp1 = r[a];
				u8 cmp2 = r[b];

				switch (d >> 3) {
					case 0: break;
					case 1: cmp1 = r[b]; cmp2 = r[a]; break;
					case 2: cmp2 = b; break;
					case 3: cmp1 = a; break;
					default: printf("Invalid comparison setting: %d\n", d >> 3);
				}

				switch (d & 7) {
					case 0: flag = 1; break;
					case 1: flag = 0; break;
					case 2: flag = (cmp1 == cmp2); break;
					case 3: flag = (cmp1 != cmp2); break;
					case 4: flag = (getSigned(cmp1) < getSigned(cmp2)); break;
					case 5: flag = (getSigned(cmp1) > getSigned(cmp2)); break;
					case 6: flag = (cmp1 < cmp2); break;
					case 7: flag = (cmp1 > cmp2); break;
				}
			} break;

			// Shift/rotate by immediate
			case 012: {
				u8 ib = b & 7;

				switch (b >> 3) {
					case 0: r[d] = r[a] << ib; break;
					case 1: r[d] = r[a] >> ib; break;
					case 2: if (r[a] & 0x20) r[d] = (0x3F << (6 - ib)) | (r[a] >> ib); else r[d] = r[a] >> ib; break;
					case 3: r[d] = (r[a] << ib) | (r[a] >> (6 - ib)); break;
					default: printf("Invalid shift setting: %d\n", b >> 3);
				}
			} break;

			// Mem access
			case 015: r[d] = r[(r[a] + b) & 0x3F]; break;
			case 016: r[(r[a] + b) & 0x3F] = r[d]; break;

			// Multiply
			case 023: {
				switch (b >> 4) {
					case 0: r[d] = ((u16) r[d] * r[a]) >> (b & 15); break;
					case 1: r[d] = ((i16) getSigned(r[d]) * getSigned(r[a])) >> (b & 15); break;
					default: printf("Invalid multiply setting: %d\n", b >> 4);
				}
			} break;

			// Control flow
			case 017: break;
			case 020: jump(64 * (u16) d + a, b, -3); break;
			case 021: jump(64 * (u16) d + a, b, 3); break;

			// I/O
			case 022: r[d] = IO(a, r[b]); break;

			default: printf("Invalid instruction: %o\n", opc);
		}

		r[0] = 0;
		r[d] &= 0x3F;
		pc += 3;

		if (pc >= tapeSize) pc -= tapeSize;
		if (pc < 0) pc += tapeSize;
	}

private:
	i8 getSigned(u8 value) {
		i8 signedValue = value;

		if (value > 31) {
			signedValue -= 64;
		}

		//printf("Signed: %o -> %o\n", value, signedValue);
		return signedValue;
	}

	void jump(u16 label, u8 rc, i8 direction) {
		//printf("Jumping to %d, dir: %i\n", label, direction);

		u8 found = 0;
		while (!found) {
			pc += direction;
			if (pc >= tapeSize) pc -= tapeSize;
			if (pc < 0) pc += tapeSize;

			u8 opc = ((tape[pc] >> 2) - 1) % 21;

			if (opc == 017) {
				u32 instruction = tape[pc] * 0x10000 + tape[pc + 1] * 0x100 + tape[pc + 2];
				u8 opc = (instruction & (0x3F << (6 * 3))) >> (6 * 3);
				u8 condition = (opc - 1) / 21;

				u8 d = (instruction & (0x3F << (6 * 2))) >> (6 * 2);
				u8 a = (instruction & (0x3F << (6 * 1))) >> (6 * 1);
				u8 b = instruction & (0x3F);

				//printf("%d: seeking label: %o,%o,%o: %d - %d\n", pc, d, a, b, label, 64 * (u16) d + a);

				if (64 * (u16) d + a == label && b == r[rc]) {
					if (condition == 0) found = 1;
					if (condition == 1 && flag == 1) found = 1;
					if (condition == 2 && flag == 0) found = 1;
				}
			}
		}

		pc -= 3;
	}

	u8 IO(u8 device, u8 value) {
		//printf("IO Access: %o %o\n", device, value);

		switch (device) {
			case 0: readBufferSize = strlen(fgets((char*) readBuffer, 64, stdin)); return readBufferSize;
			case 1: if (readBufferIndex < readBufferSize) return characterTo6bit(readBuffer[readBufferIndex++]); else return 0x3F;
			case 2: printf("%c", CHARSET[value]); fflush(stdout); break;
			case 3: if (value != 0) { clockCounter = 0; } return clockCounter & 0x3F;
			case 4: if (value != 0) { clockCounter = 0; } return (clockCounter >> 6) & 0x3F;

			case 020: addr &= ~(0x3F << 12); addr |= value << 12; break;
			case 021: addr &= ~(0x3F << 6); addr |= value << 6; break;
			case 022: addr &= ~0x3F; addr |= value; break;
			case 023: return mem[(addr++) & 0x3FFFF]; break;
			case 024: mem[(addr++) & 0x3FFFF] = value; break;

			case 025: gpu->x = value; break;
			case 026: gpu->y = value; break;
			case 027: gpu->setPixel(value); break;

			case 030: value = joypad; joypad = 0; return value;

			default: printf("Unknown I/O device: %d\n", device);
		}

		return 0;
	}

	u8 characterTo6bit(char c) {
		if (c >= 'a' && c <= 'z') {
			c += 'A' - 'a';
		}

		for (u8 i = 0; i < sizeof(CHARSET); i++) {
			if (CHARSET[i] == c) {
				return i;
			}
		}

		printf("Couldn't find character: %c\n", c);
		return 0;
	}

	u64 getCurrentTime() {
		return duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count();
	}

	void clockTick(u64 currentTime) {
		u64 tick = currentTime;

		if (tick > lastTick + 10) {
			lastTick = tick;
			clockCounter++;
		}
	}

	void processInput() {
		SDL_Event evt;

		while (SDL_PollEvent(&evt)) {
			if (evt.type == SDL_QUIT) halted = 1;

			if (evt.type == SDL_KEYDOWN || evt.type == SDL_KEYUP) {
				u8 bit = 0;

				switch (evt.key.keysym.sym) {
					case SDLK_x: bit = 32; break;
					case SDLK_y: bit = 16; break;
					case SDLK_UP: bit = 8; break;
					case SDLK_DOWN: bit = 4; break;
					case SDLK_LEFT: bit = 2; break;
					case SDLK_RIGHT: bit = 1; break;
					default: break;
				}

				if (evt.key.state == SDL_PRESSED) {
					joypad |= bit;
				} else {
					joypad &= ~bit;
				}
			}
		}
	}

	u64 lastTick;
	u64 lastInteract;

	u8 joypad;
	GPU* gpu;
	u8 mem[0x40000];
	u32 addr;

	u8 r[64];
	i32 pc;
	u8 flag;

	u8 readBufferIndex;
	u8 readBufferSize;
	u8 readBuffer[65];
	u8* tape;
	i32 tapeSize;

	u16 clockCounter = 0;
	u32 clockDivider = 0;
};
#endif