#ifndef GPU_H
#define GPU_H
#include "utils.h"

class GPU {
public:
	u8 x, y;
	u8 pixelSize = 10;

	GPU() {
		window = SDL_CreateWindow("EMU 1.0", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 64 * pixelSize, 64 * pixelSize, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB332, SDL_TEXTUREACCESS_STREAMING, 64, 64);
		SDL_RenderClear(renderer);

		x = 0;
		y = 0;

		memset(pixels, 0, sizeof(pixels));
	}

	void setPixel(u8 color) {
		u8 whiteR = (color & 0b110000) == 0b110000;
		u8 whiteG = (color & 0b1100) == 0b1100;

		color = ((color & 0b111100) << 1) | (color & 0b11);
		color = ((color & 0b1100000) << 1) | (color & 0b11111);

		if (whiteR) color |= 1 << 5;
		if (whiteG) color |= 1 << 2;

		pixels[y * 64 + x] = color;
	}

	void render() {
		SDL_UpdateTexture(texture, NULL, pixels, 64);
		SDL_RenderCopy(renderer, texture, NULL, NULL);
		SDL_RenderPresent(renderer);
	}

private:
	u8 pixels[64 * 64];

	SDL_Window* window;
	SDL_Renderer* renderer;
	SDL_Texture* texture;
};
#endif