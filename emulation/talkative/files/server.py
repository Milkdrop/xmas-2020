from pwn import *

FLAG = "X-MAS{Y0U-$UR3-AS-H#LL-4REN'T!!1!!}"

emu_charset = list("0123456789abcdefghijklmnopqrstuvwxyz +-*/<=>()[]{}#$_?|^&!~,.:\n".upper()) + [""]
assert len(emu_charset) == 64
def ascii2emu(x):
	if x not in emu_charset: x = '#'
	return emu_charset.index(x)

def emu2ascii(x):
	return emu_charset[x]

queue = []

def cb(c):
	print(f"New connection: {c.rhost}:{c.rport}")
	queue.append(c)

srv = server(2000, callback=cb, blocking=False)

def ENET_INCOMING(conn):
	try:
		conn._fillbuffer(timeout = 0.001)
	except EOFError:
		conn.close()
	return min(63, len(conn.buffer))

def ENET_RECV(conn):
	x = conn.recvn(1, timeout=0.001)
	if x == b'': return 63
	assert x[0] in range(63)
	return x[0]

def ENET_SEND(x):
	try:
		conn.send(bytes([x]))
	except EOFError:
		conn.close()
	return 0

def ENET_CONN_CTRL(o):
	global srv, conn, marked, queue

	#print(f"conn ctrl {o}")

	if o == 1:
		marked = queue[0]
		return 0
	elif o == 2:
		found = False

		while True:
			N = len(queue)
			for _ in range(N):
				queue = queue[1:] + queue[:1]
				if queue[0] == marked:
					found = True
				if queue[0].connected():
					conn = queue[0]
					return found
				else:
					print(f"Disconnected {queue[0].rhost}:{queue[0].rport}")
					queue = queue[1:]
			time.sleep(0.01)

while True:
	if len(queue) == 0:
		time.sleep(0.01)
		continue
	queue = queue[1:] + queue[:1]
	try:
		queue[0]._fillbuffer(timeout = 0.01)
		if len(queue[0].buffer) >= 40:
			msg = ''.join(emu2ascii(q) for q in queue[0].recvn(40))
			msg = f"{msg[:4]}: {msg[4:]}\n"
			if msg == "YMAS: I MIGHT BE BETTER THAN X-MAS LOLOLOL\n":
				print(f"{queue[0].rhost}:{queue[0].rport} GOT A FLAG")
				queue[0].send(bytes([ascii2emu(c) for c in FLAG]))
			else:
				print(msg, end='')
				for conn in queue[1:]:
					try:
						conn.send(bytes([ascii2emu(c) for c in msg]))
					except EOFError:
						pass
	except EOFError:
		print(f"Disconnected {queue[0].rhost}:{queue[0].rport}")
		queue[0].close()
		queue = queue[1:]
