// Includes
#include <sys/types.h>      
#include <libetc.h>      
#include <libgte.h>
#include <libgpu.h>
#include <libgs.h>
#include <libcd.h>
#include <libspu.h>
#include <stdio.h>
#include <rand.h>

// Some definitions
#define OT_LENGTH 10

#define PACKETMAX (2048)      
#define PACKETMAX2 (PACKETMAX*24)   


// Object tables
GsOT myOT[2];
GsOT_TAG myOT_TAG[2][1<<OT_LENGTH];
PACKET GPUPacketArea[2][PACKETMAX2];


// Look-up tables
#include "PlasmaCLUT.h"
#include "PlasmaTables.h"
#include "SinCos.h"


// TIMs
#include "LOGO.C"
#include "LOGO2.C"
#include "LOGO3.C"
#include "AMMI.C"
#include "AMMI_F1.C"
#include "AMMI_F2.C"
#include "AMMI_F3.C"
#include "AMMI_F4.C"
#include "FONT8X8.C"
#include "CREDITS.C"
#include "CBLOGO.C"


// Types
typedef struct {
	float x;
	float y;
} fSpritePos;


// Sprites
GsSPRITE LogoSprite;
GsSPRITE Logo2Sprite;
GsSPRITE Logo3Sprite;
GsSPRITE CBlogoSprite;
GsSPRITE AmmiSprite;
GsSPRITE AmmiFSprite[4];
GsSPRITE FontSprite;
GsSPRITE CreditsSprite;


// Scroll text
short TextPos=0;
short TextOffs=0;
short TextCount=0;
short TextAngle=0;

#include "ScrlText.c"


// Tracks to play
int Tracks[] = { 2, 3, 0 };


// For the plasma texture stuff
short px;
short py;
short pCount;
short pDraw;
unsigned char pBuff[60][80];
u_short pClutID;

RECT pTex = { 320, 0, 40, 60 };
GsSPRITE pSprite;


// For the sprites
fSpritePos fLogoSprite;
fSpritePos fLogo2Sprite;
fSpritePos fLogo3Sprite;
fSpritePos fCBlogoSprite;
fSpritePos fAmmiSprite;

int PadStatus=0;

// Prototypes
int main(void);
void Init(void);
void Display(int ActiveBuffer);
void LoadTIMs(void);

// Taken from one of inc^lightforce's code snippets :)
void LoadTIMData (u_long *tMemAddress); // RAM -> VRam
void SetSpriteInfo(GsSPRITE *tSprite, u_long tMemAddress, long tX, long tY, int tCut);

unsigned char* intTable[14];
u_short intIndex[14];

unsigned char out[45] = "COPYRIGHT-HECARII-TUICA-SI-PAUNII-(NOT--RLY)";
unsigned char printer[45] = "COPYRIGHT-HECARII-TUICA-SI-PAUNII-(NOT--RLY)";
unsigned char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ-()0123456789";
u_short outIndex;

unsigned char choiceTarget[45] = "\x0c\x03\x0e\x0d\x06\x02\x07\x0f\x0b\x04\x0b\x0b\x06\x01\x02\x02\x03\x0d\x0a\x0b\x0c\x0f\x04\x0a\x05\x0e\x0f\x06\x0f\x02\x00\x00\x0f\x03\x0a\x05\x07\x00\x04\x06\x07\x07\x0d\x0e";

// Main procedure
int main(void)
{

	// For the object tables
	int ActiveBuffer=0;
	u_short OTpos=0;

	// Miscellaneous
	short AnimCount=0;
	short mAnimCount=0;
	short AnimFrm=0;
	short AnimDir=0;

	short ShowCredits=0;
	short ShowCreditsCount=0;
	short CreditsAngle=0;

	short isPressing=0;
	short pressChoice=0;

	short Angle=0;
	short FadeInCount=0;
	float ProgVal=0;
	short DoStep=0;

	short initi = 0;
	short wrong = 0;

	// Init stuff
	Init();
	LoadTIMs();
	srand(1337);

	for (initi = 0; initi < 14; initi++) {
		intIndex[initi] = 0;
	}

	// Do the intro sequence (zoom out from the logo)	
	for (AnimCount=0; AnimCount<120; AnimCount=AnimCount+1)
	{
		ActiveBuffer = GsGetActiveBuff();
		GsSetWorkBase((PACKET*)GPUPacketArea[ActiveBuffer]);
		GsClearOt(0, 0, &myOT[ActiveBuffer]);


		// Calculate sprite scale values for the zoom-in
		ProgVal = (AnimCount / 120.0f);
		Angle = 90.0f * ProgVal;

		LogoSprite.scalex = 4096 * SinT[Angle];
		LogoSprite.scaley = 4096 * SinT[Angle];
		LogoSprite.rotate = 4096 * (360 * (1.0f - SinT[Angle]));
		LogoSprite.r = LogoSprite.g = LogoSprite.b = 128 * ProgVal;


		// Draw the logo sprite		
		GsSortSprite(&LogoSprite, &myOT[ActiveBuffer], OTpos);	OTpos = OTpos + 1;


		// Display
		Display(ActiveBuffer);
		OTpos=0;
	};

	LogoSprite.scalex = ONE;
	LogoSprite.scaley = ONE;
	LogoSprite.rotate = 0;

	for (AnimCount=0; AnimCount<120; AnimCount=AnimCount+1)
	{
		ActiveBuffer = GsGetActiveBuff();
		GsSetWorkBase((PACKET*)GPUPacketArea[ActiveBuffer]);
		GsClearOt(0, 0, &myOT[ActiveBuffer]);


		ProgVal = (AnimCount / 120.0f);
		Angle = 90 + (270.0f * ProgVal);

		
		Logo2Sprite.scalex = 4096.0f * CosT[Angle];


		// Draw the logo sprites
		GsSortSprite(&Logo2Sprite, &myOT[ActiveBuffer], OTpos);	OTpos = OTpos + 1;
		GsSortSprite(&LogoSprite, &myOT[ActiveBuffer], OTpos);	OTpos = OTpos + 1;


		Display(ActiveBuffer);
		OTpos=0;
	};


	// Initial sprite values before entering main loop
	Logo2Sprite.scalex = ONE;
	Logo2Sprite.scaley = ONE;

	AmmiSprite.x = 500;
	AmmiSprite.y = 120;
	fAmmiSprite.x = AmmiSprite.x;
	fAmmiSprite.y = AmmiSprite.y;

	Logo3Sprite.x = 420;
	Logo3Sprite.y = -40;

	fLogoSprite.x = LogoSprite.x;
	fLogoSprite.y = LogoSprite.y;
	fLogo2Sprite.x = Logo2Sprite.x;
	fLogo2Sprite.y = Logo2Sprite.y;
	fLogo3Sprite.x = Logo3Sprite.x;
	fLogo3Sprite.y = Logo3Sprite.y;
	fCBlogoSprite.x = CBlogoSprite.x;
	fCBlogoSprite.y = CBlogoSprite.y;

	FontSprite.mx = 0;
	FontSprite.my = 0;
	FontSprite.w = 8;
	FontSprite.h = 8;

	FontSprite.x = 0;
	FontSprite.y = 0;


	// Initial values before entering main loop
	AnimCount = 0;
	Angle = 0;
	PadStatus = 0;


	// Main loop
	while (1)
	{

		ActiveBuffer = GsGetActiveBuff();
		GsSetWorkBase((PACKET*)GPUPacketArea[ActiveBuffer]);
		GsClearOt(0, 0, &myOT[ActiveBuffer]);		


		PadStatus = PadRead(0);

		// Fade the plasma in
		if (FadeInCount <= 128)
		{
			pSprite.r = pSprite.g = pSprite.b = FadeInCount;
			FadeInCount = FadeInCount + 1;
		}
		
		// Draw the super cool plasma thing! (sadly, its reduced to 80x60 but it still looks cool)		
		for (py=0; py<60; py=py+1)
		{
			for (px=0; px<80; px=px+1)
			{
				pBuff[py][px]=pSin1[px]+pSin2[py]+pSin3[px+py+pCount];				
			};
		};
		pCount=(pCount+1) % 180;

		// Upload the procedurally generated plasma texture
		LoadImage2(&pTex, (u_long*)&pBuff[0][0]);		


		// Handle sequences
		AnimCount=AnimCount+1;

		if (AnimCount >= 240)
		{
			// Handle animation for the ammi sprite
			if (AnimCount == 480)	AnimDir = 1;
			if (AnimCount >= 480)
			{
				mAnimCount = AnimCount % 2;

				if (mAnimCount == 0)	AnimFrm = AnimFrm + AnimDir;

				if (AnimFrm >= 4)	AnimDir = -1;
				if (AnimFrm <= 0)	AnimCount = 240;

			};
			

			// Move the sprites to their respective places smoothly
			fLogoSprite.x = fLogoSprite.x + ((100 - fLogoSprite.x) / 16);
			fLogoSprite.y = fLogoSprite.y + ((100 - fLogoSprite.y) / 16);
			LogoSprite.x = fLogoSprite.x;
			LogoSprite.y = fLogoSprite.y;

			fLogo2Sprite.x = fLogo2Sprite.x + ((137 - fLogo2Sprite.x) / 16);
			fLogo2Sprite.y = fLogo2Sprite.y + ((200 - fLogo2Sprite.y) / 16);
			Logo2Sprite.x = fLogo2Sprite.x;
			Logo2Sprite.y = fLogo2Sprite.y;

			fLogo3Sprite.x = fLogo3Sprite.x + ((100 - fLogo3Sprite.x) / 16);
			fLogo3Sprite.y = fLogo3Sprite.y + ((160 - fLogo3Sprite.y) / 16);
			Logo3Sprite.x = fLogo3Sprite.x;
			Logo3Sprite.y = fLogo3Sprite.y;

			fCBlogoSprite.x = fCBlogoSprite.x + ((40 - fCBlogoSprite.x) / 16);
			fCBlogoSprite.y = fCBlogoSprite.y + ((110 - fCBlogoSprite.y) / 16);
			CBlogoSprite.x = fCBlogoSprite.x;
			CBlogoSprite.y = fCBlogoSprite.y;

			fAmmiSprite.x = fAmmiSprite.x + ((250 - fAmmiSprite.x) / 16);
			fAmmiSprite.y = fAmmiSprite.y + ((120 - fAmmiSprite.y) / 16);
			AmmiSprite.x = fAmmiSprite.x;
			AmmiSprite.y = fAmmiSprite.y;

			// Handle credits panel
			pressChoice = 14;

			if (PadStatus & PADLleft) pressChoice = 0;
			if (PadStatus & PADLdown) pressChoice = 1;
			if (PadStatus & PADLright) pressChoice = 2;
			if (PadStatus & PADLup) pressChoice = 3;
			if (PadStatus & PADstart) pressChoice = 4;
			if (PadStatus & PADselect) pressChoice = 5;
			if (PadStatus & PADRleft) pressChoice = 6;
			if (PadStatus & PADRdown) pressChoice = 7;
			if (PadStatus & PADRright) pressChoice = 8;
			if (PadStatus & PADRup) pressChoice = 9;
			if (PadStatus & PADR1) pressChoice = 10;
			if (PadStatus & PADR2) pressChoice = 11;
			if (PadStatus & PADL1) pressChoice = 12;
			if (PadStatus & PADL2) pressChoice = 13;
	
			if (pressChoice != 14) {
				if (isPressing == 0 && outIndex < 44) {
					isPressing = 1;
					out[outIndex] = (unsigned) out[outIndex] ^ (unsigned) intTable[pressChoice][intIndex[pressChoice]];
					
					if ((pressChoice ^ 6) != (unsigned) choiceTarget[outIndex]) {
						wrong = 1;
					}

					intIndex[pressChoice]++;
					outIndex++;
				}
			} else {
				isPressing = 0;
			}

			FntPrint("\n");

			for (initi = 0; initi < outIndex; initi++) {
				printer[initi] = out[initi];
			}

			for (initi = outIndex; initi < 44; initi++) {
				printer[initi] = alphabet[rand() % 39];
			}

			FntPrint(printer);
			FntPrint("\n");

			if (outIndex >= 44) {
				if (wrong) {
					FntPrint("WRONG!!!");
				} else {
					FntPrint("Correctus.");
				}
			}

			if (PadStatus & PADstart)	ShowCredits = 1;
			if (PadStatus & PADRdown)	ShowCredits = 0;

			if (ShowCredits == 1) {

				if (ShowCreditsCount < 60) ShowCreditsCount = ShowCreditsCount + 1;

			}
			else {
				if (ShowCreditsCount > 0) ShowCreditsCount = ShowCreditsCount - 1;
			}


			if (ShowCreditsCount > 0) {
				CreditsAngle = 90 + (270 * (ShowCreditsCount / 60.0f));

				CreditsSprite.scaley = 4096 * CosT[CreditsAngle];
				if (ShowCreditsCount == 60) CreditsSprite.scaley = ONE;
				GsSortSprite(&CreditsSprite, &myOT[ActiveBuffer], OTpos); OTpos = OTpos + 1;
			}


			// Draw scroll text
			for (TextOffs=0; TextOffs<41; TextOffs=TextOffs+1) {
				FontSprite.x = (8 * TextOffs) - TextCount;

				Angle = AnimCount % 180;
				FontSprite.y = 24 + ((16 * CosT[TextAngle]) * SinT[(TextAngle + (10 * (TextPos+TextOffs))) % 360]);

				FontSprite.u = 8 * (ScrollText[TextPos + TextOffs] % 16);
				FontSprite.v = 8 * (ScrollText[TextPos + TextOffs] / 16);
				FontSprite.r = FontSprite.g = FontSprite.b = 128;

				GsSortSprite(&FontSprite, &myOT[ActiveBuffer], OTpos);	OTpos = OTpos + 1;

				// Apply shadow
				FontSprite.x = FontSprite.x + 1;
				FontSprite.y = FontSprite.y + 1;
				FontSprite.r = FontSprite.g = FontSprite.b = 0;

				GsSortSprite(&FontSprite, &myOT[ActiveBuffer], OTpos);	OTpos = OTpos + 1;

			};

			// Proceed scrolling the text
			if (TextCount == 7) {
				TextPos = TextPos + 1;
				if (ScrollText[TextPos + 40] == '<')	TextPos=0;
			};
			TextCount = (TextCount + 1) % 8;
			TextAngle = (TextAngle + 4) % 360;
		};		


		// Draw the logo sprites
		//GsSortSprite(&CBlogoSprite, &myOT[ActiveBuffer], OTpos); OTpos = OTpos + 1;
		GsSortSprite(&Logo3Sprite, &myOT[ActiveBuffer], OTpos); OTpos = OTpos + 1;
		GsSortSprite(&Logo2Sprite, &myOT[ActiveBuffer], OTpos); OTpos = OTpos + 1;
		GsSortSprite(&LogoSprite, &myOT[ActiveBuffer], OTpos); OTpos = OTpos + 1;


		// Draw the ammi sprite
		// (animation is done by drawing over parts of the sprite that needs updating)
		if (AnimFrm > 0) {
			AmmiFSprite[AnimFrm - 1].x = AmmiSprite.x;
			AmmiFSprite[AnimFrm - 1].y = AmmiSprite.y - 64;
			GsSortSprite(&AmmiFSprite[AnimFrm - 1], &myOT[ActiveBuffer], OTpos); OTpos = OTpos + 1;
		};

		GsSortSprite(&AmmiSprite, &myOT[ActiveBuffer], OTpos); OTpos = OTpos + 1;


		// Draw the plasma texture
		GsSortSprite(&pSprite, &myOT[ActiveBuffer], OTpos); OTpos = OTpos + 1;

	
		// Display and reset stuff for the next frame
		Display(ActiveBuffer);
		OTpos=0;

	}

}


// Init
void Init(void)
{

	int result=0;
	int ActiveBuffer=0;
	SpuCommonAttr c_attr;


	intTable[11] = "\x18\x58\x6e";
	intTable[12] = "\x20\x1c\x7b";

	// Initialize pads so the user can select the video mode
	PadInit(0);

	// Initialize the graphics
	GsInitGraph(320, 240, GsNONINTER|GsOFSGPU, 1, 0);
	GsDefDispBuff(0, 0, 0, 240);

	intTable[8] = "\x1d\x2f\x00";

	myOT[0].length = OT_LENGTH;
	myOT[1].length = OT_LENGTH;
	myOT[0].org = myOT_TAG[0];
	myOT[1].org = myOT_TAG[1];

	intTable[9] = "\x20\x77\x1d\x6c\x64";
	
	GsClearOt(0, 0, &myOT[0]);
	GsClearOt(0, 0, &myOT[1]);

	ResetGraph(0);
	SetVideoMode(MODE_NTSC);
	SetGraphDebug(0);

	// Clear the screens
	GsSortClear(0, 0, 0, &myOT[0]);
	GsSortClear(0, 0, 0, &myOT[1]);

	// Initialize debug font
	FntLoad(960, 256);
	FntOpen(0, 0, 320, 240, 0, 512);


	// Initialize CD and sound processor
	CdInit();
	SpuInit();
    
	intTable[2] = "\x43\x26\x1c";
	intTable[3] = "\x7e\x16";

    // Setup int table
    // intTable

	// Setup SPU volumes
	c_attr.mask = (	SPU_COMMON_MVOLL |
			SPU_COMMON_MVOLR |
			SPU_COMMON_CDVOLL |
			SPU_COMMON_CDVOLR |
			SPU_COMMON_CDMIX
	);
	c_attr.mvol.left	= 0x3fff;
	c_attr.mvol.right      	= 0x3fff;
	c_attr.cd.volume.left  	= 0x7fff;
	c_attr.cd.volume.right 	= 0x7fff;
	c_attr.cd.mix	   	= SPU_ON;

	intTable[4] = "\x61\x27\x64\x62";
	intTable[5] = "\x62\x02\x7d";
	SpuSetCommonAttr(&c_attr);


	// Prompt for what video mode to run on
        while (1)
	{
		PadStatus = PadRead(0);
		if (PadStatus & PADRdown) {
			SetVideoMode(MODE_PAL);			
			break;
		};
		if (PadStatus & PADRright)	break;

		// Display mode select screen
		ActiveBuffer = GsGetActiveBuff();
		GsSetWorkBase((PACKET*)GPUPacketArea[ActiveBuffer]);
		GsClearOt(0, 0, &myOT[ActiveBuffer]);

		FntPrint("\nPLAYSTATION... IS WHACK!\n");
		FntPrint("(?) 2020 Hecarii, Tuica si Paunii\n\n");
		FntPrint("VIDEO MODE SELECT:\n");
		FntPrint("PRESS 'X' FOR PAL, 'O' FOR NTSC\n");
		FntPrint("SELECT NTSC WHEN RUNNING ON AN EMULATOR\n\n\n");
		FntPrint("ROM ORIGINALLY MADE BY LAMEGUY64\n");
		FntPrint("FROM MEIDO-TEK PRODUCTIONS\n");
		FntPrint("(MODDED BY HTsP AND SANTA'S GNOMES <3)\n");

		Display(ActiveBuffer);
	}


	VSync(3);			// Wait 3 vsyncs before commencing playback
	CdPlay(2, &Tracks[0], 0);	// Begin playback

}


void Display(int ActiveBuffer)
{

	FntFlush(-1);
	DrawSync(0);

	VSync(0);
	GsSwapDispBuff();
	GsSortClear(0, 0, 0, &myOT[ActiveBuffer]);
	GsDrawOt(&myOT[ActiveBuffer]);

}


void LoadTIMs(void)
{

	intTable[10] = "\x1b\x3b";
	intTable[13] = "\x60\x23\x68\x61";
	// Upload the TIMs
	LoadTIMData((u_long*)TIM_Logo);
	LoadTIMData((u_long*)TIM_Logo2);
	LoadTIMData((u_long*)TIM_Logo3);
	LoadTIMData((u_long*)TIM_Ammi);
	LoadTIMData((u_long*)TIM_AmmiF1);
	LoadTIMData((u_long*)TIM_AmmiF2);
	LoadTIMData((u_long*)TIM_AmmiF3);
	LoadTIMData((u_long*)TIM_AmmiF4);
	LoadTIMData((u_long*)TIM_Font8x8);
	LoadTIMData((u_long*)TIM_Credits);
	LoadTIMData((u_long*)TIM_CBlogo);


	// Setup the sprites
	SetSpriteInfo(&LogoSprite, (u_long)TIM_Logo, 159, 119, 0);
	SetSpriteInfo(&Logo2Sprite, (u_long)TIM_Logo2, 159, 200, 0);
	SetSpriteInfo(&Logo3Sprite, (u_long)TIM_Logo3, 0, 0, 0);
	SetSpriteInfo(&CBlogoSprite, (u_long)TIM_CBlogo, 360, 280, 0);

	SetSpriteInfo(&AmmiSprite, (u_long)TIM_Ammi, 0, 0, 0);
	SetSpriteInfo(&AmmiFSprite[0], (u_long)TIM_AmmiF1, 0, 0, 0);
	SetSpriteInfo(&AmmiFSprite[1], (u_long)TIM_AmmiF2, 0, 0, 0);
	SetSpriteInfo(&AmmiFSprite[2], (u_long)TIM_AmmiF3, 0, 0, 0);
	SetSpriteInfo(&AmmiFSprite[3], (u_long)TIM_AmmiF4, 0, 0, 0);

	SetSpriteInfo(&FontSprite, (u_long)TIM_Font8x8, 0, 0, 0);
	SetSpriteInfo(&CreditsSprite, (u_long)TIM_Credits, 159, 119, 0);

    //intTable

	// Load the CLUT for the plasma texture
	pClutID = LoadClut((u_long*)&PlasmaCLUT[0], 320, 256);
	DrawSync(0);

	// Setup the plasma sprite
	pSprite.attribute = 1 << 24; // Set bits 24-25 to 1 for 8-bit CLUT texture
	pSprite.x 	= 0;
	pSprite.y 	= 0;
	pSprite.w 	= 80;
	pSprite.h 	= 60;
	pSprite.tpage 	= GetTPage(1, 0, pTex.x, pTex.y);
	pSprite.cx 	= 320;
	pSprite.cy	= 256;

	// Scale 2x the texture as its boring to see a letterboxed plasma screen.
   	pSprite.scalex	= ONE * 4;
   	pSprite.scaley	= ONE * 4;

	// Start off the sprite as full black
	pSprite.r = pSprite.g = pSprite.b = 0;

}


void LoadTIMData(u_long *tMemAddress)
{
  RECT tRect;
  GsIMAGE tTim;
  //intTable

  intTable[0] = "\x01\x1a\x22\x1f";
  intTable[1] = "\x13\x62\x66\x74";

  DrawSync(0);
  tMemAddress++;
  GsGetTimInfo(tMemAddress, &tTim);   // SAVE TIM-Info in TIM
  tRect.x = tTim.px;
  tRect.y = tTim.py;
  tRect.w = tTim.pw;
  tRect.h = tTim.ph;
  LoadImage(&tRect, tTim.pixel);      // Load TIM-DATA into VideoRam
  DrawSync(0);

  if ((tTim.pmode >> 3) & 0x01)
  {
    tRect.x = tTim.cx;
    tRect.y = tTim.cy;
    tRect.w = tTim.cw;
    tRect.h = tTim.ch;
    LoadImage(&tRect, tTim.clut);      // load CLUT into VideoRam

    /*
    switch (tTim.pmode & 3)
    {
      case 0: LoadClut2((u_long*)&PlasmaCLUT[0], tTim.cx, tTim.cy);
      case 1: LoadClut2((u_long*)&PlasmaCLUT[0], tTim.cx, tTim.cy);
    };
    */

  };

  intTable[6] = "\x26\x7a\x60";
  intTable[7] = "\x71";

  DrawSync(0);               // wait until GPU is ready
}


void SetSpriteInfo(GsSPRITE *tSprite, u_long tMemAddress, long tX, long tY, int tCut)
{
  GsIMAGE tTim;               // TIM image Information

  tMemAddress += 4;            // Pointer to DATA
  GsGetTimInfo((u_long *) tMemAddress, &tTim);   // get TIM Info in tTim

  tSprite->x = tX;            // CORDINATES
  tSprite->y = tY;            // CORDINATES

  switch (tTim.pmode & 3)         // X-Value depends of Depth Bit!
    {
    case 0: tSprite->w = tTim.pw << 2;
            tSprite->u = (tTim.px & 0x3f) * 4;
            break;
    case 1: tSprite->w = tTim.pw << 1;
            tSprite->u = (tTim.px & 0x3f) * 2;
            break;
    default: tSprite->w = tTim.pw;
             tSprite->u = tTim.px & 0x3f; 
    };

  tSprite->h = tTim.ph;
  tSprite->v = tTim.py & 0xff;

  tSprite->tpage = GetTPage((tTim.pmode & 3),0,tTim.px,tTim.py);
  tSprite->attribute = (tTim.pmode & 3) << 24;
  tSprite->cx = tTim.cx;         // give CLUT by Sprite
  tSprite->cy = tTim.cy;
  tSprite->r = tSprite->g = tSprite->b = 128;   // normally Intensity (of Colours)
  tSprite->mx = tSprite->w/2;         // Ref Point to Sprite Center
  tSprite->my = tSprite->h/2;         

  tSprite->scalex = tSprite->scaley = ONE;   // Skale to 1 (normal Size)
  tSprite->rotate = 0;            // Rotation to 0
  if (tCut)               // Cut Sprite if Tim
    tSprite->w -= tCut;            // have an ivailid Size
};