import os
import sys
from text import *
from test_gen import *
import threading
import time

def die(timeout):
    time.sleep(timeout)
    os._exit(0)

def is_number(x):
    for c in x:
        if not c in "0123456789":
            return False
    return True

max_time = 60
test_cnt = 100
modulo = 666013
generator = test_generator()
print (intro.format(test_cnt, max_time))

thr = threading.Thread (target = die, args = (max_time,))
thr.start ()

for i in range(test_cnt):
        print (test_no.format(i + 1, test_cnt))
        sys.stdout.flush()
        x, a, c, mod, n, k, p, q, ans = generator.get_test(max(10, (i + 1) ** 3), 10**9 // 100 ** 2 * (i + 1) ** 2)
        print(present_test.format(n, k, p, q, x, a, c, mod))
        sys.stdout.flush()
        resp = input()

        if not is_number(resp):
            print (bad_input)
            sys.stdout.flush()
            exit()
        resp = int(resp)
        
        if resp != ans:
            print (wrong_answer)
            sys.stdout.flush()
            exit()
        else:
            print (good_answer)
            sys.stdout.flush()
            
print (win.format(FLAG))
sys.stdout.flush()
