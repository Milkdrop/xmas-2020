test_no = "Test number: {}/{}"

wrong_answer = "That is not the correct answer!"

good_answer = "Good, thats right!"

intro = "Hey, we've got a problem at the factory! As you well know we have N rooms in our factory, and in the i-th room we have v[i] gifts that have to be built.\nAlso, at the factory we have N mechagnomes of two possible types:\n\tK of them are the Fast-O-Bot type, which can build P gifts in a single day\n\tThe rest of N - K bots are the Speed-O-Tron type which can build Q gifts in a single day.\n\nEach day a mechagnome is assigned to a room, and that day it'll work all by itself in that room, building as many gifts as it can.\nIf there are no gifts to be built or he finishes all of them before the end of the day, the mechagnome goes idle. There cannot be two mechagnomes in the same room in the same day.\nSince we're in a hurry, we need the minimum number of days we can build all gifts. Can you help us?\nJust to be safe, we have to solve {} such scenarios. We don't want to waste any time so we'll give you {} seconds to solve everything.\nAh, and since our connection is so slow, we'll define v in the following way: v[i] = (a * v[i - 1] + c) % mod for all i = 2, n\n"

bad_input = "Bad input, I was expecting an integer. Aborting!"

present_test = "N = {}, K = {}\nP = {}, Q = {}\nv[1] = {}, a = {}, c = {}, mod = {}\n"

win = "Thanks for saving Christmas this year!\nHere's the flag: {}"

FLAG = "X-MAS{l0l_h0w_15_7h1s_4_b1n4ry_s34rch_pr0bl3m?}"
