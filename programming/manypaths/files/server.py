import os
import sys
from text import *
from test_gen import *
import threading
import time

def print_test(n, mat, blockers, dist):
	m_out = '\n'.join([','.join([str(a) for a in mat[i]]) for i in range(len(mat))])
	blockers_out = ','.join([str(a) for a in blockers])
	print(present_test.format(n, m_out, blockers_out, dist), flush = True)

def die(timeout):
    time.sleep(timeout)
    os._exit(0)

def is_number(x):
    for c in x:
        if not c in "0123456789":
            return False
    return True

max_time = 45
test_cnt = 40
modulo = 666013
generator = test_generator(modulo)
print (intro.format(test_cnt, max_time, modulo), flush = True)

thr = threading.Thread (target = die, args = (max_time,))
thr.start ()

for i in range(test_cnt):
        print (test_no.format(i + 1, test_cnt))
        n, mat, blockers, distance, ans = generator.get_test(i + 5, int((i + 5) ** 0.33), int(i + 5 + (i + 1) ** 3))
        print_test(n, mat, blockers, distance)
        x = input()

        if not is_number(x):
            print (bad_input, flush = True)
            exit()
        x = int(x)
        
        if x != ans:
            print (wrong_answer, flush = True)
            exit()
        else:
            print (good_answer, flush = True)
            
print (win.format(FLAG), flush = True)
sys.stdout.flush()
