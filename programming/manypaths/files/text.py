test_no = "Test number: {}/{}"

wrong_answer = "That is not the correct answer!"

good_answer = "Good, thats right!"

intro = "I swear that Santa is going crazy with those problems, this time we're really screwed!\nThe new problem asks us the following:\nGiven an undirected graph of size N by its adjacency matrix and a set of forbidden nodes, tell me how many paths from node 1 to node N of exactly length L that don't pass through any of the forbidden nodes exist (please note that a node can be visited multiple times)?\nAnd if that wasn't enough, we need to answer {} of those problems in {} seconds and to give each output modulo {}. What does that even mean!?\n"

bad_input = "Bad input, I was expecting an integer. Aborting!"

present_test = "N = {}\nadjacency matrix:\n{}\nforbidden nodes: [{}]\nL = {}\n"

win = "I cannot believe you figured this one out, how does this code even work?\nI'm baffled, here's the flag: {}"

FLAG = "X-MAS{n0b0dy_3xp3c73d_th3_m47r1x_3xp0n3n71a7i0n}"
