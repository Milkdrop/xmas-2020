from random import SystemRandom
from heapq import *

class test_generator:
    def __init__(self):
        self.rnd = SystemRandom()

    def get_test(self, size, minsize, maxval):
        l1 = self.rnd.randint(minsize, minsize * 4)
        l2 = self.rnd.randint(minsize, minsize * 4)
        arr = []
        ans1 = []
        ans2 = []
        h1 = []
        h2 = []
        heapify(h1)
        heapify(h2)
        
        for i in range(size):
            x = self.rnd.randint(1, maxval)
            arr.append(x)
            heappush(h1, -x)
            if len(h1) > l1:
                heappop(h1)

            heappush(h2, x)
            if len(h2) > l2:
                heappop(h2)

        while len(h1) > 0:
            ans1.append(-heappop(h1))
        ans1 = list(reversed(ans1))

        while len(h2) > 0:
            ans2.append(heappop(h2))
        ans2 = list(reversed(ans2))

        return arr, l1, l2, ans1, ans2
