import os
import sys
from text import *
from test_gen import *
import threading
import time

def die(timeout):
    time.sleep(timeout)
    os._exit(0)

max_time = 45
test_cnt = 50
generator = test_generator()
print (intro.format(test_cnt, max_time))

thr = threading.Thread (target = die, args = (max_time,))
thr.start ()

for i in range(test_cnt):
    print (test_no.format(i + 1, test_cnt))
    arr, l1, l2, ans1, ans2 = generator.get_test(int(5 * (i + 1)**1.5), (i + 1), 10 * (i + 1))
    print (present_test.format(arr, l1, l2))
    try:
        k1, k2 = input().split(";")
        k1 = k1.split(",")
        k2 = k2.split(",")
        if len(k1) != l1 or len(k2) != l2:
            print (bad_input)
            exit()
        for i in range(l1):
            k1[i] = int(k1[i])
        for i in range(l2):
            k2[i] = int(k2[i])

    except:
        print (bad_input)
        exit()

    if ans1 != k1 or ans2 != k2:
        print (wrong_answer)
        exit()
    else:
        print (good_answer)

print (win.format(FLAG))
sys.stdout.flush()
